from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from schema.models import Schema, DataSetSchema


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'id', 'username')


class SchemaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schema
        fields = ('id', 'params', 'updated', 'title')


class SchemaViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schema
        fields = ('id', 'updated', 'title')


class DataSetSchemaSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataSetSchema
        fields = ('id', 'created', 'status', 'file_csv')


class LogInSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        user_data = UserSerializer(user).data
        for key, value in user_data.items():
            if key != 'id':
                token[key] = value
        return token
