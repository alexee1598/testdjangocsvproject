from django.urls import path
from channels.routing import ProtocolTypeRouter, URLRouter

from testDjangoCSVProject.middleware import TokenAuthMiddlewareStack
from schema.consumers import NotificationConsumer


application = ProtocolTypeRouter({
    'websocket': TokenAuthMiddlewareStack(
        URLRouter([
            path('csv-status/', NotificationConsumer.as_asgi()),
        ])
    ),
})
