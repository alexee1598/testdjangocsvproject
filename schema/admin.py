from django.contrib import admin

from schema.models import Schema, DataSetSchema


@admin.register(Schema)
class SchemaAdmin(admin.ModelAdmin):
    list_display = ('title', 'params')


@admin.register(DataSetSchema)
class SchemaAdmin(admin.ModelAdmin):
    list_display = ('created', 'status', 'file_csv')
