from channels.generic.websocket import AsyncJsonWebsocketConsumer


class NotificationConsumer(AsyncJsonWebsocketConsumer):
    groups = ['test']
    channel_group_name = 'core-data'

    async def connect(self):
        user = self.scope['user']

        if user.is_anonymous:
            await self.close()
        else:
            await self.channel_layer.group_add(self.channel_group_name, channel=self.channel_name)
            await self.accept()

    async def disconnect(self, code):
        user = self.scope['user']
        if user.is_anonymous:
            await self.close()
        else:
            await self.channel_layer.group_discard(self.channel_group_name, self.channel_name)

        await super().disconnect(code)

    async def send_my_data(self, event):
        text = event['data']
        await self.send(text_data=text)
