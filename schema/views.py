import logging

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView

from schema.helper import valid_schema_params
from schema.models import Schema, DataSetSchema
from schema.serializers import LogInSerializer, SchemaSerializer, DataSetSchemaSerializer, SchemaViewSerializer
from schema.tasks import create_csv_file_job

logger = logging.getLogger(__name__)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def schema_board_view(request):
    schema_list = Schema.objects.all().values('id', 'updated', 'title')
    return Response({
        'schemaList': SchemaViewSerializer(schema_list, many=True).data,
    })


@api_view(['GET', 'DELETE', 'POST'])
@permission_classes([IsAuthenticated])
def schema_view(request, schema_pk):
    if request.method == 'DELETE':
        try:
            Schema.objects.get(pk=schema_pk).delete()
            return Response({
                'result': 1,
            })
        except Exception as e:
            logging.error('schema_view DELETE - ', e)
            return Response({
                'result': 0,
            })
    if request.method == 'GET':
        try:
            s = Schema.objects.get(pk=schema_pk)
            return Response({
                'schema': SchemaSerializer(s).data,
                'result': 1,
            })
        except Exception as e:
            logging.error('schema_view GET - ', e)
            return Response({
                'result': 0,
            })
    if request.method == 'POST':
        try:
            s = Schema.objects.get(pk=schema_pk)
            if valid_schema_params(request.data):
                s.title = request.data.get('name')
                s.params = request.data
                s.save()
                return Response({
                    'result': 1,
                })
            else:
                return Response({
                    'result': 0,
                    'error': 'Incorrect Data'
                })
        except Exception as e:
            logging.error('schema_view POST - ', e)
            return Response({
                'result': 0,
            })


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_schema(request):
    try:
        if str(request.data.get('name')).strip() == '' or request.data.get('name') is None:
            raise Exception
        s = Schema()
        s.title = request.data['name']
        s.params = request.data
        s.save()
        return Response({
            'result': 1,
        })
    except Exception as e:
        logging.error('create_schema - ', e)
        return Response({
            'result': 0,
        })


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def generate_csv(request, schema_pk, count):
    try:
        s = Schema.objects.get(pk=schema_pk)
        d = DataSetSchema()
        d.status = 'PROCESSING'
        d.schemas = s
        d.save()
        create_csv_file_job.delay(schema_pk, count, d.id)
        d = DataSetSchema.objects.filter(schemas_id=schema_pk)
        return Response({
            'result': 1,
            'dataSets': DataSetSchemaSerializer(d, many=True).data,
        })
    except Exception as e:
        logging.error('generate_csv - ', e)
        return Response({
            'result': 0,
        })


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_generate_csv(request, csv_pk):
    try:
        t = DataSetSchema.objects.get(pk=csv_pk)
        return Response({
            'test': f'https://test-bucket-python-alexee151.s3.eu-north-1.amazonaws.com/media/{t.file_csv}',
            'result': 1,
        })
    except Exception as e:
        logging.error('get_generate_csv', e)
        return Response({
            'result': 0,
        })


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def data_sets_board(request, schema_pk):
    d = DataSetSchema.objects.filter(schemas_id=schema_pk)
    return Response({
        'result': 1,
        'dataSets': DataSetSchemaSerializer(d, many=True).data,
    })


class LogInView(TokenObtainPairView):
    serializer_class = LogInSerializer
