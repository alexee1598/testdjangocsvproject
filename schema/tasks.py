import json
import logging

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from testDjangoCSVProject.celery import app

from schema.helper import _export_to_csv
from schema.models import Schema

logger = logging.getLogger(__name__)


@app.task
def create_csv_file_job(schema_pk: int, count: int, data_schema_id: int) -> None:
    try:
        schema = Schema.objects.get(pk=schema_pk)
        status = _export_to_csv(schema, count, data_schema_id)
        message = {'type': 'send_my_data',
                   'data': json.dumps({'status': status})}
        async_to_sync(get_channel_layer().group_send)('core-data', message)
        logger.info('create_csv_file_job - success')
        return None
    except Exception as e:
        logger.error('create_csv_file_job ', e)
        return None
