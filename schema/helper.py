import csv
import logging
import os
import random
import re

from datetime import timedelta, datetime
from typing import List

from django.core.files import File
from rest_framework.response import Response

from schema.models import Schema, DataSetSchema
from schema.shema_data.data import variants

logger = logging.getLogger(__name__)


def _export_to_csv(schema: Schema, count: int, data_schema_id: int) -> str:
    first_name_var = variants.get('first_name')
    last_name_var = variants.get('last_name')
    company_var = variants.get('company')
    job_var = variants.get('job')
    address_var = variants.get('address')
    domain_var = variants.get('domain')
    email_var = variants.get('email')
    number_var = variants.get('number_code')
    row_name = ''
    d_schema = ''
    try:
        name = re.search("'name': '([\\w]+)',", schema.params)
        delimiter = re.search("'column_separator': '(.?)',", schema.params).group(1)
        list_row = schema.params.split('{')
        len_table = len(list_row) - 2

        res = _parse_data(len_table, list_row)

        for index, i in enumerate(sorted(res.values(), key=lambda x: int(x['order']))):
            row_name += i['column_name'] if index == len(res) - 1 else i['column_name'] + f'{delimiter}'

        d_schema = DataSetSchema.objects.get(pk=data_schema_id)

        with open(f'media/{name.group(1)}_{schema.id}_{d_schema.id}.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            data_row = [row_name]
            for index in range(count):
                data_row.append('')
                for index2, i in enumerate(sorted(res.values(), key=lambda x: int(x['order']))):
                    if i['type'] == 'Integer':
                        data_row[index + 1] += str(random.randint(int(i.get('from_range')), int(i.get('to'))))
                    elif i['type'] == 'Full name':
                        data_row[index + 1] += f'{random.choice(first_name_var)} {random.choice(last_name_var)}'
                    elif i['type'] == 'Company':
                        data_row[index + 1] += str(random.choice(company_var))
                    elif i['type'] == 'Job':
                        data_row[index + 1] += str(random.choice(job_var))
                    elif i['type'] == 'Address':
                        data_row[index + 1] += str(random.choice(address_var))
                    elif i['type'] == 'Domain name':
                        data_row[index + 1] += str(random.choice(domain_var))
                    elif i['type'] == 'Email':
                        data_row[index + 1] += f'{random.randint(1, 2000)}{random.choice(email_var)}'
                    elif i['type'] == 'Date':
                        data_row[index + 1] += f'{random_date()}'
                    elif i['type'] == 'Phone number':
                        code = random.choice(number_var)
                        if 1 <= len(f'{code}') <= 2:
                            data_row[index + 1] += f'+{code} ({random.randint(100, 999)})' \
                                                   f' {random.randint(100, 999)} - {random.randint(1000, 9999)}'
                        elif len(f'{code}') == 3:
                            data_row[index + 1] += f'+{str(code)[0:2]} ({str(code)[2]}{random.randint(10, 99)})' \
                                                   f' {random.randint(100, 999)} - {random.randint(1000, 9999)}'
                    if index2 != len(res) - 1:
                        data_row[index + 1] += f'{delimiter}'
            for line in data_row:
                writer.writerow(line.split(","))
    except Exception as e:
        d_schema.status = 'ERROR'
        d_schema.save()
        logger.error('_export_to_csv', e)
        return d_schema.status
    else:
        with open(f'media/{name.group(1)}_{schema.id}_{d_schema.id}.csv', 'rb') as file:
            d_schema.status = 'READY'
            d_schema.file_csv = File(file, name=os.path.basename(file.name))
            d_schema.save()
        os.remove(f'media/{name.group(1)}_{schema.id}_{d_schema.id}.csv')
    return d_schema.status


def random_date() -> datetime:
    start = datetime.strptime('1/1/2008 1:30 PM', '%m/%d/%Y %I:%M %p')
    end = datetime.strptime('1/1/2021 4:50 AM', '%m/%d/%Y %I:%M %p')
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + timedelta(seconds=random_second)


def valid_schema_params(params: Response) -> bool:
    if params.get('name', '') == '' or params.get('name', '') == 'None':
        return False
    qd = str(dict(params))
    list_row = qd.split('{')
    len_table = len(list_row) - 2
    try:
        for i, d in enumerate(_parse_data(len_table, list_row).values()):
            if d.get('column_name') is None or '':
                return False
            if d.get('type') is None or '':
                return False
            if d.get('order') is None or 0 or '':
                return False
    except Exception as e:
        logger.error(e)
        return False
    else:
        return True


def _parse_data(len_table: int, list_row: List[str]) -> dict:
    i = 0
    res = {}

    while i < len_table:
        column_name = re.search("'column_name': '([\\w ]+)',", list_row[i + 2]).group(1)
        column_type = re.search("'type': '([\\w ]+)',", list_row[i + 2]).group(1)
        from_range = re.search("'from': '?([\\d None]+)'?", list_row[i + 2]).group(1)
        to = re.search("'to': '?([\\d None]+)'?", list_row[i + 2]).group(1)
        order = re.search("'order': '?([\\d]+)'?", list_row[i + 2]).group(1)
        res[i] = {'column_name': column_name, 'type': column_type,
                  'from_range': from_range, 'to': to, 'order': order}
        i += 1
    return res
