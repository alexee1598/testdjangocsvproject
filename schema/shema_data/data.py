variants = {
    'first_name': ('Alexander', 'Alexey', 'Valeriy', 'Nikolay', 'Michael', 'Vitaliy', 'Sergey', 'Cab', 'Cabell',
                   'Cable', 'Cace', 'Cacey', 'Caddaric', 'Cade', 'Caden', 'Cadman', 'Cadmus', 'Cadwell', 'Caedmon',
                   'Caelan', 'Caelen', 'Caellum', 'Caelum', 'Caelus', 'Caesar', 'Caesarae'),

    'last_name': ('Kirillov', 'Tolstoy', 'Karenin', 'Alexeev', 'Artemov', 'Gubarev', 'Kuchinov'),

    'company': ('Microsoft', 'AMD', 'Salesforce', 'Amazon', 'Oracle', 'Nvidia', 'J&J', 'Quest'),

    'job': ('Mechanic', 'Welder', 'Teacher', 'Driver', 'Police officer', 'Programmer', 'Designer', 'Constructor'),

    'address': ('777 Brockton Avenue Abington MA 2351', '30 Memorial Drive Avon MA 2322',
                '250 Hartford Avenue Bellingham MA 2019', '700 Oak Street Brockton MA 2301',
                '66-4 Parkhurst Rd Chelmsford MA 1824', '591 Memorial Dr Chicopee MA 1020',
                '55 Brooksby Village Way Danvers MA 1923', '137 Teaticket Hwy East Falmouth MA 2536',
                '42 Fairhaven Commons Way Fairhaven MA 2719', '374 William S Canning Blvd Fall River MA 2721'),

    'domain': ('google.com', 'yahoo.com', 'youtube.com', 'yandex.ru', 'vk.com', 'facebook.com'),

    'email': ('terrhr@gehg.com', 'rthrth@gmail.com', 'tgrhtyh234@gmail.com', 'hytj@gmail.com', 'gikhowemm@gmail.com'),

    'number_code': (1, 7, 20, 29, 32, 48, 49, 60, 380),
}
