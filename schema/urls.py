from django.urls import path

from schema import views
from schema.views import LogInView

urlpatterns = [
    path('login/', LogInView.as_view(), name='login'),
    path('create-schema/', views.create_schema, name='create-schema'),
    path('detail/<int:schema_pk>/', views.schema_view),
    path('view/<int:schema_pk>/', views.schema_view, name='edit-schema'),
    path('generate-csv/', views.schema_view),
    path('generate-csv/<int:schema_pk>/<int:count>/', views.generate_csv),
    path('get-generate-csv/<int:csv_pk>/', views.get_generate_csv),
    path('data-sets-board/<int:schema_pk>/', views.data_sets_board),
    path('', views.schema_board_view, name='index-view'),
]
