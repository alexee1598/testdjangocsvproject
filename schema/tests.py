import base64
import json
import logging

from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from schema.models import Schema

PASSWORD = 'pAssw0rd!'
SCHEMA_TITLE = 'Test Schema'


def create_user(username='user@example.com', password=PASSWORD):
    user = get_user_model().objects.create_user(username=username, password=password)
    user.save()
    return user


class AuthenticationTest(APITestCase):

    def test_log_in(self):
        user = create_user()
        response = self.client.post(reverse('login'), {'username': user.username, 'password': PASSWORD})
        access = response.data['access']
        header, payload, signature = access.split('.')
        decoded_payload = base64.b64decode(f'{payload}==')
        payload_data = json.loads(decoded_payload)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertIsNotNone(response.data['refresh'])
        self.assertEqual(payload_data['id'], user.id)
        self.assertEqual(payload_data['username'], user.username)
        self.assertEqual(payload_data['first_name'], user.first_name)
        self.assertEqual(payload_data['last_name'], user.last_name)


class SchemaTest(APITestCase):

    def setUp(self):
        logging.disable(logging.ERROR)
        self.user = create_user()
        self.client.login(username=self.user.username, password=PASSWORD)
        s = Schema(title=SCHEMA_TITLE,
                   params="{'name': 'Tertghrth', 'column_separator': ',', 'string_character': '\"' selling_points': ["
                          "{'column_name': 'Full name2', 'type': 'Full name', 'from': 'None', 'to': 'None', "
                          "'order': '2'}, {'column_name': 'Address5', 'type': 'Address', 'from': 'None', "
                          "'to': 'None', 'order': '5'}]}")
        s.save()

    def test_get_schema_board_view(self):
        response = self.client.get(reverse('index-view'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data.get('schemaList')), 1)
        self.assertEqual(response.data.get('schemaList')[0].get('title'), 'Test Schema')
        self.assertEqual(response.data.get('schemaList')[0].get('params'), None)
        self.assertNotEqual(response.data.get('schemaList')[0].get('id'), '4444')

    def test_create_schema_correct(self):
        response = self.client.post(reverse('create-schema'),
                                    {'name': 'test_schema',
                                     'column_separator': ',',
                                     'string_character': '\"', 'selling_points': [{'column_name': 'Full name2', 'type': 'Full name', 'from': 'None', 'to': 'None', 'order': '2'}]})
        schemas = Schema.objects.all().count()
        schema = Schema.objects.get(pk=2)
        print(schema.params)
        print(schema.title)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(schemas, 2)
        self.assertEqual(response.data.get('result'), 1)

    def test_create_schema_incorrect_data(self):
        response = self.client.post(reverse('create-schema'),
                                    {'name': '',
                                     'params': "{'name': 'Table', 'column_separator': ',', 'string_character': '\"' "
                                               "selling_points': [{'column_name': 'Full name2', 'type': 'Full name', "
                                               "'from': 'None', 'to': 'None', 'order': '2'}"})
        schemas = Schema.objects.all().count()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(schemas, 1)
        self.assertEqual(response.data.get('result'), 0)

    def test_schema_view_delete_correct(self):
        schema_counter = Schema.objects.all().count()
        self.assertEqual(schema_counter, 1)

        response = self.client.delete('/detail/1/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('result'), 1)

        schema_counter = Schema.objects.all().count()

        self.assertEqual(schema_counter, 0)

    def test_schema_view_delete_incorrect_id(self):
        schema_counter = Schema.objects.all().count()
        self.assertEqual(schema_counter, 1)

        response = self.client.delete('/detail/124124/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('result'), 0)

        schema_counter = Schema.objects.all().count()

        self.assertEqual(schema_counter, 1)

    def test_schema_view_get_correct(self):
        schema_id = 1
        response = self.client.get(f'/detail/{schema_id}/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data.get('schema'), None)
        self.assertNotEqual(response.data.get('schema').get('params'), None)
        self.assertEqual(response.data.get('schema').get('id'), schema_id)
        self.assertEqual(response.data.get('result'), 1)

    def test_schema_view_get_incorrect_id(self):
        response = self.client.get(f'/detail/5656/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('schema'), None)
        self.assertEqual(response.data.get('result'), 0)

    def test_schema_view_post_correct(self):
        schema_params_before = Schema.objects.get(pk=1).params

        response = self.client.post(f'/detail/1/',
                                    {'name': 'test_schemag',
                                     'column_separator': ',',
                                     'string_character': '\"', 'selling_points': [{'column_name': 'Full sdfsdfname', 'type': 'Full name', 'from': 'None', 'to': 'None', 'order': '2'}]})

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        expected = Schema.objects.get(pk=1).params
        self.assertNotEqual(schema_params_before, expected)
        self.assertEqual(response.data.get('result'), 1)

    def test_schema_view_post_incorrect_id(self):
        schema_params_before = Schema.objects.get(pk=1).params

        response = self.client.post(f'/detail/14441/',
                                    {'name': 'test_schema',
                                     'column_separator': ',',
                                     'string_character': '\"',
                                     'selling_points': [{'column_name': 'Full name2', 'type': 'Full name',
                                                         'from': 'None', 'to': 'None', 'order': '2'}]})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = Schema.objects.get(pk=1).params
        self.assertEqual(schema_params_before, expected)
        self.assertEqual(response.data.get('result'), 0)

    def test_schema_view_post_incorrect_data(self):
        schema_params_before = Schema.objects.get(pk=1).params

        response = self.client.post(f'/detail/1/',
                                    {'name': 'None',
                                     'column_separator': ',',
                                     'string_character': '\"', 'selling_points': [{'column_name': 'thrthr', 'type': 'Full name', 'from': 'None', 'to': 'None', 'order': '2'}]})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = Schema.objects.get(pk=1).params
        self.assertEqual(schema_params_before, expected)
        self.assertEqual(response.data.get('result'), 0)
        self.assertEqual(response.data.get('error'), 'Incorrect Data')

    def test_data_sets_board_correct_id(self):
        response = self.client.get(f'/data-sets-board/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('result'), 1)
        self.assertNotEqual(response.data.get('dataSets'), '')

    def test_data_sets_board_incorrect_id(self):
        response = self.client.get(f'/data-sets-board/144/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('result'), 1)
        self.assertEqual(response.data.get('dataSets'), [])

