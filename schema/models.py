from django.db import models

from testDjangoCSVProject.storage_backends import PublicMediaStorage


class Schema(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=32, blank=False, null=False)
    params = models.TextField(null=True, blank=True, )


class DataSetSchema(models.Model):
    PROCESSING = 'PROCESSING'
    ERROR = 'ERROR'
    READY = 'READY'

    STATUS_CHOICES = (
        (PROCESSING, PROCESSING),
        (ERROR, ERROR),
        (READY, READY),
    )
    file_csv = models.FileField(upload_to='csv_schemas',
                                null=True,
                                blank=True,
                                )
    created = models.DateTimeField(auto_now_add=True, )
    status = models.CharField(max_length=30,
                              choices=STATUS_CHOICES,
                              default=PROCESSING,
                              )
    schemas = models.ForeignKey(Schema, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.id}, {self.schemas}'

    class Meta:
        ordering = ('-created',)
